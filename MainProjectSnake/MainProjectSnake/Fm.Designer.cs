﻿
namespace MainProjectSnake
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showScore = new System.Windows.Forms.Label();
            this.showPoint = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // showScore
            // 
            this.showScore.AutoSize = true;
            this.showScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.showScore.Location = new System.Drawing.Point(13, 13);
            this.showScore.Name = "showScore";
            this.showScore.Size = new System.Drawing.Size(98, 21);
            this.showScore.TabIndex = 0;
            this.showScore.Text = "Score: 0";
            // 
            // showPoint
            // 
            this.showPoint.AutoSize = true;
            this.showPoint.BackColor = System.Drawing.Color.Red;
            this.showPoint.Location = new System.Drawing.Point(27, 128);
            this.showPoint.Name = "showPoint";
            this.showPoint.Size = new System.Drawing.Size(21, 21);
            this.showPoint.TabIndex = 1;
            this.showPoint.Text = " ";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1000, 500);
            this.Controls.Add(this.showPoint);
            this.Controls.Add(this.showScore);
            this.Font = new System.Drawing.Font("Courier New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Fm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label showScore;
        private System.Windows.Forms.Label showPoint;
    }
}

