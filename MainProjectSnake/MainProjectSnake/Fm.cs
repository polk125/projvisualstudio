﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainProjectSnake
{
    public partial class Fm : Form
    {
        int colls = 50;
        int rows = 25;
        int score = 0;
        int dx = 0;
        int dy = 0;
        int front = 0;
        int back = 0;
        Part[] snake = new Part[1250];
        List<int> available = new List<int>();
        bool[,] visit;
        Random rand = new Random();

        Timer timer = new Timer();
        public Fm()
        {
            InitializeComponent();
            intial();
            launchTimer();
        }

        private void launchTimer()
        {
            timer.Interval = 75;
            timer.Start();
            timer.Tick += move;

        }

        private void move(object sender, EventArgs e)
        {
            int x = snake[front].Location.X, y = snake[front].Location.Y;
            if (dx == 0 && dy == 0)
                return;
            if (collisionPoint(x + dx, y + dy))
            {
                score += 1;
                showScore.Text = "Score: " + score.ToString();
                if (hits((y + dy) / 20, (x + dx) / 20))
                {
                    this.Close();
                    return;
                }
                Part head = new Part(x + dx, y + dy);
                front = (front - 1 + 1250) % 1250;
                snake[front] = head;
                visit[head.Location.Y / 20, head.Location.X / 20] = true;
                Controls.Add(head);
                randomFood();
            }
            else
            {
                if (hits((y + dy) / 20, (x + dx) / 20))
                {
                    this.Close();
                    return;
                }
                visit[snake[back].Location.Y / 20, snake[back].Location.X / 20] = false;
                front = (front - 1 + 1250) % 1250;
                snake[front] = snake[back];
                snake[front].Location = new Point(x + dx, y + dy);
                back = (back - 1 + 1250) % 1250;
                visit[(y + dy) / 20, (x + dx) / 20] = true;
            }
        }

        private void randomFood()
        {
            available.Clear();
            for (int i = 0; i < rows; i++)
                for(int j = 0; j < colls; j++)
                    if (!visit[i, j]) available.Add(i * colls + j);
            int idx = rand.Next(available.Count) % available.Count;
            showPoint.Left = (available[idx] * 20) % Width;
            showPoint.Top = (available[idx] * 20) % Height;
        }

        private bool hits(int x, int y)
        {
            try
            {
                if (visit[x, y])
                {
                    timer.Stop();
                    MessageBox.Show("Змея задела себя");
                    return true;
                }
                return false;
            }
            catch
            {
                timer.Stop();
                MessageBox.Show("Змея задела стенку");
                return true;
            }
            
        }

        private bool collisionPoint(int x, int y)
        {
            return x == showPoint.Location.X && y == showPoint.Location.Y;
        }

        private void intial()
        {
            visit = new bool[rows, colls];
            Part head = new Part((rand.Next() % colls) * 20, (rand.Next() % rows) * 20);
            showPoint.Location = new Point((rand.Next() % colls) * 20, (rand.Next() % rows) * 20);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colls; j++)
                {
                    visit[i, j] = false;
                    available.Add(i * colls + j);
                }
                visit[head.Location.Y / 20, head.Location.X / 20] = true;
                available.Remove(head.Location.Y / 20 * colls + head.Location.X / 20);
                Controls.Add(head); snake[front] = head;
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            dx = dy = 0;
            switch (e.KeyCode)
            {
                case Keys.Right:
                    dx = 20;
                    break;
                case Keys.Left:
                    dx = -20;
                    break;
                case Keys.Up:
                    dy = -20;
                    break;
                case Keys.Down:
                    dy = 20;
                    break;
            }
        }
    }
}
