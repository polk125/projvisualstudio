﻿namespace labButtonsOnGrid
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSpace = new System.Windows.Forms.Panel();
            this.rowsPlus = new System.Windows.Forms.Button();
            this.rowsMinus = new System.Windows.Forms.Button();
            this.collsMinus = new System.Windows.Forms.Button();
            this.collsPlus = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rowsNumb = new System.Windows.Forms.Label();
            this.collsNumb = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainSpace
            // 
            this.mainSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainSpace.Location = new System.Drawing.Point(189, 0);
            this.mainSpace.Name = "mainSpace";
            this.mainSpace.Size = new System.Drawing.Size(613, 451);
            this.mainSpace.TabIndex = 0;
            // 
            // rowsPlus
            // 
            this.rowsPlus.Location = new System.Drawing.Point(148, 31);
            this.rowsPlus.Name = "rowsPlus";
            this.rowsPlus.Size = new System.Drawing.Size(35, 23);
            this.rowsPlus.TabIndex = 1;
            this.rowsPlus.Text = "+";
            this.rowsPlus.UseVisualStyleBackColor = true;
            // 
            // rowsMinus
            // 
            this.rowsMinus.Location = new System.Drawing.Point(13, 31);
            this.rowsMinus.Name = "rowsMinus";
            this.rowsMinus.Size = new System.Drawing.Size(37, 23);
            this.rowsMinus.TabIndex = 2;
            this.rowsMinus.Text = "-";
            this.rowsMinus.UseVisualStyleBackColor = true;
            // 
            // collsMinus
            // 
            this.collsMinus.Location = new System.Drawing.Point(13, 105);
            this.collsMinus.Name = "collsMinus";
            this.collsMinus.Size = new System.Drawing.Size(37, 23);
            this.collsMinus.TabIndex = 2;
            this.collsMinus.Text = "-";
            this.collsMinus.UseVisualStyleBackColor = true;
            // 
            // collsPlus
            // 
            this.collsPlus.Location = new System.Drawing.Point(148, 105);
            this.collsPlus.Name = "collsPlus";
            this.collsPlus.Size = new System.Drawing.Size(35, 23);
            this.collsPlus.TabIndex = 1;
            this.collsPlus.Text = "+";
            this.collsPlus.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Количетсво строк";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Количество столбцов";
            // 
            // rowsNumb
            // 
            this.rowsNumb.Location = new System.Drawing.Point(77, 35);
            this.rowsNumb.Name = "rowsNumb";
            this.rowsNumb.Size = new System.Drawing.Size(38, 15);
            this.rowsNumb.TabIndex = 3;
            this.rowsNumb.Text = "3";
            this.rowsNumb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // collsNumb
            // 
            this.collsNumb.Location = new System.Drawing.Point(77, 109);
            this.collsNumb.Name = "collsNumb";
            this.collsNumb.Size = new System.Drawing.Size(38, 19);
            this.collsNumb.TabIndex = 3;
            this.collsNumb.Text = "4";
            this.collsNumb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.collsNumb);
            this.Controls.Add(this.rowsNumb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.collsPlus);
            this.Controls.Add(this.collsMinus);
            this.Controls.Add(this.rowsMinus);
            this.Controls.Add(this.rowsPlus);
            this.Controls.Add(this.mainSpace);
            this.Name = "Fm";
            this.Text = "labButtonsOnGrid";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mainSpace;
        private System.Windows.Forms.Button rowsPlus;
        private System.Windows.Forms.Button rowsMinus;
        private System.Windows.Forms.Button collsMinus;
        private System.Windows.Forms.Button collsPlus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label rowsNumb;
        private System.Windows.Forms.Label collsNumb;
    }
}

