﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;

        public int Rows { get; private set; } = 3;
        public int Colls { get; private set; } = 4;
        

        public Fm()
        {
            InitializeComponent();
            CreateButtons();
            ReSizeButtons();
            mainSpace.Resize += (s, e) => ReSizeButtons();
            collsPlus.Click += CollsPlus_Click;
            collsMinus.Click += CollsMinus_Click;
            rowsPlus.Click += RowsPlus_Click;
            rowsMinus.Click += RowsMinus_Click;

        }

        private void RowsMinus_Click(object sender, EventArgs e)
        {
            if (Rows != 1)
            {
                Rows--;
                CreateButtons();
                ReSizeButtons();
                rowsNumb.Text = Rows.ToString();
            }
        }

        private void RowsPlus_Click(object sender, EventArgs e)
        {
            if (Rows != 10)
            {
                Rows++;
                CreateButtons();
                ReSizeButtons();
                rowsNumb.Text = Rows.ToString();
            }
        }

        private void CollsMinus_Click(object sender, EventArgs e)
        {
            if (Colls != 1)
            {
                Colls--;
                CreateButtons();
                ReSizeButtons();
                collsNumb.Text = Colls.ToString();
            }
        }

        private void CollsPlus_Click(object sender, EventArgs e)
        {
            if (Colls != 10)
            {
                Colls++;
                CreateButtons();
                ReSizeButtons();
                collsNumb.Text = Colls.ToString();
            }
        }

        private void ReSizeButtons()
        {
            int x = mainSpace.ClientSize.Width / Colls;
            int y = mainSpace.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Colls; j++)
                {
                    bu[i, j].Width = x;
                    bu[i, j].Height = y;
                    bu[i, j].Location = new Point(x * j, y * i);
                }
            }
        }

        private void CreateButtons()
        {
            mainSpace.Controls.Clear();
            bu = new Button[Rows, Colls];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Colls; j++)
                {
                    bu[i, j] = new Button();
                    var Button = bu[i, j];
                    bu[i, j].MouseEnter += (s,e) => Button.BackColor = Color.Red;
                    bu[i, j].MouseLeave += (s,e) => Button.BackColor = Color.White;
                    bu[i, j].Text = (i * Colls + 1 + j).ToString();
                    mainSpace.Controls.Add(bu[i, j]);
                }
            }
        }
    }
    }
