﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            button2.Click += Button2_Click;

            button3.Click += delegate
            {
                MessageBox.Show("Способ 3");
            };
            button4.Click += (s, e) => button4.BackColor = Color.Red;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 2");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 1");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
