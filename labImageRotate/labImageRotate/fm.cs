﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class labImageRotate : Form
    {
        private readonly Bitmap ImHero;

        public labImageRotate()
        {
            InitializeComponent();


            ImHero = new Bitmap(Properties.Resources.Hero);

            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();
            pictureBox1.Paint += PictureBox1_Paint;

            button1.Click += (s, e) => { ImHero.RotateFlip(RotateFlipType.RotateNoneFlipX); pictureBox1.Invalidate(); };
            button2.Click += (s, e) => { ImHero.RotateFlip(RotateFlipType.RotateNoneFlipY); pictureBox1.Invalidate(); };

            //ImHero.RotateFlip(___);
            //pictureBox1.Image.RotateFlip(___);
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(ImHero.Width/2,ImHero.Height/2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(ImHero, -ImHero.Width / 2, -ImHero.Height / 2);

            e.Graphics.RotateTransform(-trackBar1.Value);
            e.Graphics.TranslateTransform(-ImHero.Width / 2, -ImHero.Height / 2);
            e.Graphics.DrawImage(ImHero, 150, 150);

        }
    }
}
