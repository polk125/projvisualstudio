﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollHorz
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imBg1;
        private Bitmap imBg2;
        private int deltaX;
        private Point StartPoint;
        private double xSpeed = 3;
        private int deltaX2;

        public Fm()
        {
            InitializeComponent();

            imBg1 = Properties.Resources.fon15;
            imBg2 = Properties.Resources.fon13;
            this.Height = imBg1.Height+imBg2.Height;
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBg(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => StartPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += (s, e) => xSpeed = 3;
            this.Resize += (s, e) => this.Invalidate();
            //HW: 
            // (Поиск фонов по запросу)
            // добивить в проект несколько фонов и  переключение между фонами
            // избавиться от i < 3 в For, выбрать картинку фона с маленькой шириной
            // ускорение перемещения по кнопкам
            // добавить объектов или еще несколько фонов.

            //Изменения:
            //Изменен способ прорисовки картинок (i < 3)
            //Добавлено ускорение прокрутки для кнопок
            //Добавленно изменение фона(Стрелка вверх/ вниз)
            //Фоны разделены на задний и передний план с разной скоростью скроллинга
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            if (xSpeed < 25)
            {
                xSpeed += 0.2;
            }
            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDeltaX((int)xSpeed);
                    this.Invalidate();
                    break;
                case Keys.Right:
                    UpdateDeltaX(-(int)xSpeed);
                    this.Invalidate();
                    break;
                case Keys.Up:
                    imBg1 = Properties.Resources.fon21;
                    imBg2 = Properties.Resources.fon22;
                    this.Invalidate();
                    break;
                case Keys.Down:
                    imBg1 = Properties.Resources.fon15;
                    imBg2 = Properties.Resources.fon13;
                    this.Invalidate();
                    break;
            }
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - StartPoint.X);
                StartPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX = {deltaX}, v = {v}, speed = {xSpeed}";
            deltaX += v;
            deltaX2 += v-2;
            if (deltaX > 0)
            {
                deltaX -= imBg1.Width;
            }
            else if (deltaX < -imBg1.Width)
            {
                deltaX += imBg1.Width;
            }
            if (deltaX2 > 0)
            {
                deltaX2 -= imBg2.Width;
            }
            else if (deltaX2 < -imBg2.Width)
            {
                deltaX2 += imBg2.Width;
            }
        }

        private void UpdateBg()
        {
            g.Clear(SystemColors.Control);
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Width / imBg1.Width + 2; i++)
            {
                g.DrawImage(imBg1, deltaX + i*imBg1.Width, 0);
                g.DrawImage(imBg2, deltaX2 + i * imBg1.Width, imBg1.Height);
            }
        }
    }
}
