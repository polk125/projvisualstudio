﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    public partial class Form1 : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imBg1;
        private Bitmap imBg2;
        private int deltaX;
        private Point StartPoint;
        private double xSpeed = 3;
        private int deltaX2;

        public Form1()
        {
            InitializeComponent();

            imBg1 = Properties.Resources.road;
            imBg2 = Properties.Resources.car;
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
            this.Width = imBg1.Width+200;
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBg(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => StartPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;
            this.KeyUp += (s, e) => xSpeed = 3;
            this.Resize += (s, e) => this.Invalidate();
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            if (xSpeed < 25)
            {
                xSpeed += 0.2;
            }
            switch (e.KeyCode)
            {
                case Keys.Up:
                    UpdateDeltaX((int)xSpeed);
                    this.Invalidate();
                    break;
                case Keys.Down:
                    UpdateDeltaX(-(int)xSpeed);
                    this.Invalidate();
                    break;
                case Keys.Left:
                    if(deltaX2>-100)
                    deltaX2 -= 100;
                    this.Invalidate();
                    break;
                case Keys.Right:
                    if (deltaX2 < 100)
                        deltaX2 += 100;
                    this.Invalidate();
                    break;
                    //case Keys.Up:
                    //    imBg1 = Properties.Resources.fon21;
                    //    imBg2 = Properties.Resources.fon22;
                    //    this.Invalidate();
                    //    break;
                    //case Keys.Down:
                    //    imBg1 = Properties.Resources.fon15;
                    //    imBg2 = Properties.Resources.fon13;
                    //    this.Invalidate();
                    //    break;
            }
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - StartPoint.X);
                StartPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX = {deltaX}, v = {v}, speed = {xSpeed}";
            deltaX += v;
            if (deltaX > 0)
            {
                deltaX -= imBg1.Height;
            }
            else if (deltaX < -imBg1.Height)
            {
                deltaX += imBg1.Height;
            }
            //if (deltaX2 > 0)
            //{
            //    deltaX2 -= imBg2.Height;
            //}
            //else if (deltaX2 < -imBg2.Height)
            //{
            //    deltaX2 += imBg2.Height;
            //}
        }

        private void UpdateBg()
        {
            g.Clear(SystemColors.Control);
            for (int i = 0; i < Screen.PrimaryScreen.Bounds.Height / imBg1.Height + 2; i++)
            {
                g.DrawImage(imBg1, 0, deltaX + i * imBg1.Height);
                g.DrawImage(imBg2, deltaX2 + imBg1.Width / 2 + 30, imBg1.Height/2); 
            }
        }
    }
}
