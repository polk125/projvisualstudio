﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labKey
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.KeyDown += label1_Click;
        }

        private void label1_Click(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    laText.Text = "Left";
                    break;
                case Keys.Right:
                    laText.Text = "Right";
                    break;
                case Keys.Up:
                    laText.Text = "Up";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        laText.Text = "Shift+Space";
                    }
                    else
                    {
                        laText.Text = "Space";
                    }
                    break;
                case Keys.Down:
                    laText.Text = "Down";
                    break;
                case Keys.X:
                    laText.Text = e.Shift ? "Shift+x" : "X";
                    break;
                default:
                    laText.Text = $"Другая клавиша = {e.KeyCode}";
                    break;
            }
           }
    }
}
