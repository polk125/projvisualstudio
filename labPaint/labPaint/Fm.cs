﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Fm : Form
    {
        private Pen myPenAlter;
        private readonly Bitmap b;
        private readonly Graphics g;
        private Point startPoint;
        private Pen myPen;

        public Fm()
        {
            InitializeComponent();

            myPen = new Pen(Color.Red, 10);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            myPenAlter = new Pen(Color.Blue, 10);
            myPenAlter.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            Dock.MouseDown += (s, e) => startPoint = e.Location;
            Dock.MouseMove += Dock_MouseMove;
            Dock.Paint += (s,e) => e.Graphics.DrawImage(b, 0, 0);

            trackPen.Value = Convert.ToInt32(myPen.Width);
            trackPen.ValueChanged += delegate { myPen.Width = trackPen.Value; myPenAlter.Width = trackPen.Value; };

            pxBox1.MouseClick += PxBox1_MouseClick;
            pxBox2.MouseClick += PxBox2_MouseClick; 

            btnClear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                Dock.Invalidate();
            };
            btnSave.Click += BtnSave_Click;
            btnLoad.Click += BtnLoad_Click;
            btnStars.Click += BtnStars_Click;
        }

        private void PxBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                myPen.Color = pxBox2.BackColor;
            }
            else if (e.Button == MouseButtons.Right)
            {
                myPenAlter.Color = pxBox2.BackColor;
            }
            else if (e.Button == MouseButtons.Middle)
            {
                ColorDialog color1 = new ColorDialog();
                if (color1.ShowDialog() == DialogResult.OK)
                {
                    myPen.Color = color1.Color;
                    pxBox2.BackColor = color1.Color;
                }
            }
        }

        private void PxBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                myPen.Color = pxBox1.BackColor;
            }
            else if(e.Button == MouseButtons.Right)
            {
               myPenAlter.Color = pxBox1.BackColor;
            }
            else if(e.Button == MouseButtons.Middle)
            {
                ColorDialog color1 = new ColorDialog();
                if (color1.ShowDialog() == DialogResult.OK)
                {
                    myPen.Color = color1.Color;
                    pxBox1.BackColor = color1.Color;
                }
            }
        }

        private void BtnStars_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1,10),
                    rnd.Next(1, 10)
                    );
            }
            Dock.Invalidate();
        }

        private void BtnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() ==DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                Dock.Invalidate();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Image Files(*.JPG)|*.JPG";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                b.Save(dialog.FileName);
            }
        }

        private void Dock_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                Dock.Invalidate();
            }else if(e.Button == MouseButtons.Right)
            {
                g.DrawLine(myPenAlter, startPoint, e.Location);
                startPoint = e.Location;
                Dock.Invalidate();
            }
        }

       
    }
}
