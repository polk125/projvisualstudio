﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Fm : Form
    {
        private readonly Game g;

        public Fm()
        {
            InitializeComponent();
            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            btnYes.Click += (s, e) => g.DoAnswer(true);
            btnNo.Click += (s, e) => g.DoAnswer(false);
            Renew.Click += (s, e) => g.DoReset();
            btnSkip.Click += (s, e) => g.DoSkip();
        }

        private void G_Change(object sender, EventArgs e)
        {
            correct.Text = $"Верно = {g.CountCorrect}";
            wrong.Text = $"Не верно = {g.CountWrong}";
            text.Text = g.CodeText;
            difficult.Text = $"Уровень: {g.lvlNum}";
        }
    }
}
