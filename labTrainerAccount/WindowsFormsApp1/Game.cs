﻿using System;

namespace WindowsFormsApp1
{
    internal class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public int lvlNum { get; private set; }
        

        private bool answerCorrects;
        private int difficultValue = 20;
        private int lvlUpNumber = 0;
        private int lvlDownNumber = 0;
        public int lvl = 0;

        public event EventHandler Change;
        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            lvl = 0;
            lvlDownNumber = 0;
            lvlUpNumber = 0;
            difficultValue = 20;
            DoContinue();

        }

        internal void DoSkip()
        {
            DoContinue();
        }

        private void DoContinue()
        {
            lvlNum = lvl;
            Random rnd = new Random();
            int xValue1 = rnd.Next(difficultValue);
            int xValue2 = rnd.Next(difficultValue);
            int typeTest = rnd.Next(4);
            int xResult = xValue1 + xValue2;
            string xMark = "+";
            if(typeTest == 1)
            {
                xResult = xValue1 - xValue2;
                xMark = "-";
            }
            else if(typeTest == 2){
                xResult = xValue1 * xValue2;
                xMark = "*";
            }
            else if(typeTest == 3)
            {
                if (xValue2 == 0)
                    xValue2 = rnd.Next(1,7);
                xResult = xValue1 / xValue2;
                xMark = "/";
            }
            int xResultShow = xResult;

            if (rnd.Next(2) == 1)
            {
                xResultShow += rnd.Next(1, 7) * rnd.Next(2) == 1 ? 1 : -1;
            }
            answerCorrects = xResult == xResultShow;
            CodeText = $"{xValue1} {xMark} {xValue2} = {xResultShow}";
            Change?.Invoke(this, EventArgs.Empty);
        }


        public void DoAnswer(bool v)
        {
            if (v == answerCorrects)
            {
                CountCorrect++;
                lvlUpNumber++;
            }
            else
            {
                CountWrong++;
                lvlDownNumber++;
            }
            if (lvlUpNumber == 5)
            {
                if (lvl != 11)
                {
                    lvl++;
                    lvlUpNumber = 0;
                    difficultValue += 5;
                }
            }
            
            if(lvlDownNumber == 5)
            {
                if (lvl != 0)
                {
                    lvl--;
                    lvlDownNumber = 0;
                    difficultValue -= 5;
                }
            }
            DoContinue();
        }
    }
}